import React from "react";

import Homepage from "./contact/Homepage";
import { Route, Routes } from "react-router-dom";
import Contact from "./contact";
import AddContact from "./contact/AddContact";

const App = () => {
  return (
    <>
      <Contact />
      <Routes>
        <Route exact path="/" element={<Homepage />} />
        <Route exact path="/addcontact" element={<AddContact />} />
      </Routes>

    </>
  );
};

export default App;
