import {createStore}  from 'redux'
import citydetails  from './cityDetails/Reducer'

const store = createStore( citydetails)
export default store