import React from "react";
import {Link} from "react-router-dom";

const Contact = () => {
  return (
    <div
      style={{
        textAlign: "center",
        fontSize: "x-large",
        fontWeight: "600",
        background: "#00ffff",
        height: "50px",
      }}
    >
      <Link to="/" style={{margin: "50px", textDecoration: "none"}}>
        Homepage
      </Link>
      <Link to="/addcontact" style={{margin: "50px", textDecoration: "none"}}>
        AddContact
      </Link>
    </div>
  );
};

export default Contact;
