import React, {useState} from "react";


const AddContact = () => {
  const [details, setDetails] = useState({
    id: new Date().getTime().toString(),
    name: "",
    phone: "",
    type: "",
    whatsapp: "",
  });
  const [isEdit, setIsEdit] = useState(true);
  const [editId, setEditId] = useState(null);

  let key, value;
  const handleDetails = (e) => {
    key = e.target.name;
    value = e.target.value;

    setDetails({...details, [key]: value});
  };
  var data = JSON.parse(localStorage.getItem("contactdetails") || "[]");

  const handleSubmit = () => {
    if (isEdit) {
      data.push(details);
      localStorage.setItem("contactdetails", JSON.stringify(data));
    } else {
      data.map((elem, index) => {
        if (elem.id === editId) {
          setDetails({
            id: new Date().getTime().toString(),
            name: elem.name,
            phone: elem.phone,
            type: elem.type,
            whatsapp: elem.whatsapp,
          });

          return data.splice(index, 1, details);
        }

        return elem;
      });
      localStorage.setItem("contactdetails", JSON.stringify(data));
      setEditId(null);
      setIsEdit(true);
    }
  };

  const GroupProgressBar = data.map((group) => {
    const {id, name, phone, type, whatsapp} = group;

    const handleEdit = (id) => {
      const editlist = data.find((elem, index) => {
        return elem.id === id;
      });
      console.log("editdata", editlist);
      setDetails({
        id: editlist.id,
        name: editlist.name,
        phone: editlist.phone,
        type: editlist.type,
        whatsapp: editlist.whatsapp,
      });

      setIsEdit(false);
      setEditId(id);
    };

    const handleDelete = (id) => {
      const updatedlist = data.filter((elem, index) => {
        return elem.id !== id;
      });
      console.log("updatedlist", updatedlist);
      localStorage.setItem("contactdetails", JSON.stringify(updatedlist));
      setDetails(updatedlist);
      data.push(details);
    };
    return (
      <div
        style={{
          textAlign: "center",
          border: " 5px solid black",
          width: "15%",
          margin: "auto auto 10px auto",
          padding: "10px",
          background:'#f3f3f3'
        }}
      >
        <div style={{display: "flex", flexDirection: "row"}}>
          <button
            onClick={() => handleEdit(id)}
            style={{width: "28px", padding: "0", marginRight: "140px", background: "#00ffff"}}
          >
            Edit
          </button>
          <button
            onClick={() => handleDelete(id)}
            style={{width: "38px", padding: "0",  background: "#00ffff"}}
          >
            Trash
          </button>
        </div>
        <span>
          <div>Name:-{name}</div>
          <div>Phone :-{phone}</div>
          <div>Number Type:-{type}</div>
          <div>Valid for whatsapp:-{whatsapp}</div>
        </span>
      </div>
    );
  });

  return (
    <>
      <div
        style={{
          textAlign: "center",
          border: " 5px solid black",
          width: "50%",
          margin: "30px auto auto auto",
        }}
      >
        <h3>CONTACT LIST</h3>

        <form onSubmit={handleSubmit}>
          <label> Name:</label>
          <br />
          <input
            type="text"
            value={details.name}
            name="name"
            placeholder="enter name"
            onChange={handleDetails}
            required
          ></input>
          <br />
          <br />
          <label> Phone Number:</label>
          <br />
          <input
            type="number"
            name="phone"
            placeholder="enter number"
            value={details.phone}
            onChange={handleDetails}
          ></input>
          <br />
          <br />
          <select value={details.type} onChange={handleDetails} name="type">
            <br />
            <br />
            <option>Select Your Phone Type:</option>
            <option value="personal">Personal</option>
            <option value="office">Office</option>
          </select>
          <br />
          <br />
          <p>Number Valid For WhatsApp</p>YES
          <input
            type="radio"
            name="whatsapp"
            value="true"
            onChange={handleDetails}
          ></input>
          NO
          <input
            type="radio"
            name="whatsapp"
            value="false"
            onChange={handleDetails}
          ></input>
          <br />
          {/* <p>add your profile picture</p>
<image src="" alt="profile picture"/><br/> */}
          <input
            type="Submit"
            value="Submit"
            style={{margin: "50px 0 10px 0", width: "50%", height: "35px"}}
          ></input>
          <br />
        </form>
      </div>
      <h3 style={{textAlign: "center"}}>User Details</h3>
      <div>
        {" "}
        {data.length === 0 ? (
          <div
            style={{
              textAlign: "center",
              border: " 5px solid black",
              width: "25%",
              margin: "auto auto 10px ",
              padding: "10px",
            }}
          >
            NO SAVED CONTACT
          </div>
        ) : (
          <div>{GroupProgressBar}</div>
        )}
      </div>
    </>
  );
};

export default AddContact;
